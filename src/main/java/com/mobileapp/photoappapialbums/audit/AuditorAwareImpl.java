package com.mobileapp.photoappapialbums.audit;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        String userName = authentication.getName();
//        return Optional.ofNullable(userName).filter(s -> !s.isEmpty());
        return Optional.ofNullable("d7e345c7-08f9-4ab1-958a-b385893d8e1a");

    }
}
