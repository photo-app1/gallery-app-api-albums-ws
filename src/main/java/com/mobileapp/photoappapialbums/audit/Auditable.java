package com.mobileapp.photoappapialbums.audit;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Getter(AccessLevel.PROTECTED)
@Setter(AccessLevel.PROTECTED)
@MappedSuperclass //you cant create entity or table of auditable, only use auditable to inherit new classes.
@EntityListeners(AuditingEntityListener.class)
public abstract class Auditable<U> {

    @CreatedDate
    @Column(name = "created_date", updatable = false, nullable = false)
    private Date createDate;

    @LastModifiedDate
    @Column(name = "last_updated_date")
    private Date updateDate;

    @CreatedBy
    @Column(name = "user_id", updatable = false, nullable = false)
    private U userId;

}
