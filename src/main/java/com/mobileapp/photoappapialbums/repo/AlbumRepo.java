package com.mobileapp.photoappapialbums.repo;

import com.mobileapp.photoappapialbums.data.Album;
import org.checkerframework.checker.nullness.Opt;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface AlbumRepo extends JpaRepository<Album, Long> {

    @Query(value = "SELECT * from albums ORDER BY name = ?1 LIMIT ?2", nativeQuery = true)
    List<Album> findAlbums(@Param("sort") String sort, @Param("limit") int limit);

    Optional<Album> findByUserIdAndAlbumId(String userId, String albumId);

    Optional<Album> findByAlbumId(String albumId);

    Page<Album> findByUserId(String userId, Pageable pageableRequest);

    @Transactional
    @Modifying
    @Query(value = "UPDATE albums SET name=?1 , description = ?2 WHERE album_id = ?3", nativeQuery = true)
    void updateAlbum(@Param("name") String name, @Param("description") String description,
                     @Param("albumId") String albumId);

    @Transactional
    Optional<Long> deleteByAlbumId(String albumId);

}


//persist(create n update) and delete queries must be under transactional annotation.