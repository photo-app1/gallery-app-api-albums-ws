package com.mobileapp.photoappapialbums.ui.controller;

import com.mobileapp.photoappapialbums.data.Album;
import com.mobileapp.photoappapialbums.shared.AlbumDto;
import com.mobileapp.photoappapialbums.service.AlbumService;
import com.mobileapp.photoappapialbums.ui.model.CreateAlbumRequestModel;
import com.mobileapp.photoappapialbums.ui.model.AlbumResponseModel;
import com.mobileapp.photoappapialbums.ui.model.UpdateAlbumRequestModel;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

@RestController
@Validated
@RequestMapping(path = "users/{userId}/albums")
public class AlbumController {

    AlbumService albumService;

    @Autowired
    public AlbumController(AlbumService albumService) {
        this.albumService = albumService;
    }

    /***
     * create new albums
     * @param albumDetails CreateAlbumRequestModel object
     * @return responseEntity object with AlbumResponseModel object in the body
     */
    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<AlbumResponseModel> createAlbum(
            @Valid @RequestBody CreateAlbumRequestModel albumDetails) {

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        AlbumDto albumDetailsDto = modelMapper.map(albumDetails, AlbumDto.class);
        AlbumResponseModel createdAlbum = albumService.createAlbum(albumDetailsDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(createdAlbum);
    }

    /***
     * get list of all albums
     * @param page page number
     * @param limit limit of albums for one page
     * @param sort sorting ASC or DSC
     * @return List of AlbumResponseModel
     */
    @GetMapping(path = "/all", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public List<AlbumResponseModel> getAlbums(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "sort", defaultValue = "ASC") String sort,
            @RequestParam(value = "limit", defaultValue = "10") int limit) {

        return albumService.getAlbums(page, sort, limit);

    }

    /***
     * get list of all albums of particular user.
     * @param userId user id
     * @param albumId album id
     * @param page page number
     * @param sort sorting ASC or DSC
     * @param limit limit of albums for one page
     * @return List of AlbumResponseModel
     */
    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public List<AlbumResponseModel> getUserAlbums(
            @PathVariable(value = "userId") @Size(min = 36, max = 36) String userId,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "sort", defaultValue = "ASC") String sort,
            @RequestParam(value = "limit", defaultValue = "10") int limit) {

        return albumService.getUserAlbums(userId, page, sort, limit);

    }

    /***
     * get particular album
     * @param userId user id
     * @param albumId album id
     * @return AlbumResponseModel object
     */
    @GetMapping(path = "/{albumId}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public AlbumResponseModel getAlbum(
            @PathVariable(value = "userId") @Size(min = 36, max = 36) String userId,
            @PathVariable(value = "albumId") @Size(min = 36, max = 36) String albumId) {

        return albumService.getAlbum(userId, albumId);
    }

    /***
     * update existing album
     * @param userId user id
     * @param albumId album id
     * @param albumDetails UpdateAlbumRequestModel object
     * @return AlbumResponseModel object
     */
    @PutMapping(path = "/{albumId}",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public AlbumResponseModel updateAlbum(
            @PathVariable(value = "userId") @Size(min = 36, max = 36) String userId,
            @PathVariable(value = "albumId") @Size(min = 36, max = 36) String albumId,
            @Valid @RequestBody UpdateAlbumRequestModel albumDetails) {

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        Album persistAlbum = modelMapper.map(albumDetails, Album.class);

        return albumService.updateAlbum(userId, albumId, persistAlbum);
    }

    /***
     * delete particular album.
     * @param albumId album id
     * @return Long value of number of record deleted
     */
    @DeleteMapping(path = "/{albumId}")
    public ResponseEntity<String> deleteAlbum(@PathVariable(value = "albumId")
                                                  @Size(min = 36, max = 36) String albumId) {

        Long count = albumService.deleteAlbum(albumId);
        return ResponseEntity.status(200).body(count + " record has been deleted successfully!");
    }

}