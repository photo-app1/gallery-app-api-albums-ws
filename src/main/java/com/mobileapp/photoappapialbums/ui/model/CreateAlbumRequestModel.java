package com.mobileapp.photoappapialbums.ui.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CreateAlbumRequestModel implements Serializable {

    private static final long serialVersionUID = 6234514076062840441L;

    @NotNull(message = "name can't be null")
    @Size(min = 3, message = "album name must be greater than 3 characters")
    private String name;

    @NotNull(message = "description can't be null")
    @Size(min = 5, message = "album description must be greater than 5 characters")
    private String description;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}