package com.mobileapp.photoappapialbums.exception;

import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;

public class AlbumServiceException extends RuntimeException {

    private static final long serialVersionUID = 4848572539857591021L;
    private HttpStatus statusCode;

    public AlbumServiceException(String message, @Nullable HttpStatus statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }
}
