package com.mobileapp.photoappapialbums.exception.handler;

import com.mobileapp.photoappapialbums.exception.AlbumServiceException;
import com.mobileapp.photoappapialbums.ui.model.ErrorMsgResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
@Log4j2
public class AppExceptionsHandler extends ResponseEntityExceptionHandler {

    private static final String MSG = "exception found: {}";

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handleGeneralExceptions(Exception ex, WebRequest request) {

        log.error(MSG, ex.getLocalizedMessage());

        String errorMsgDescription = ex.getLocalizedMessage();
        if (errorMsgDescription == null) errorMsgDescription = ex.toString();
        ErrorMsgResponse errorMsg = new ErrorMsgResponse(new Date(), errorMsgDescription);

        return new ResponseEntity<>(errorMsg, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @ExceptionHandler(value = {AlbumServiceException.class})
    public ResponseEntity<Object> handleAlbumServiceExceptions(Exception ex, WebRequest request) {

        log.error(MSG, ex.getLocalizedMessage());

        String errorMsgDescription = ex.getLocalizedMessage();
        if (errorMsgDescription == null) errorMsgDescription = ex.toString();
        ErrorMsgResponse errorMsg = new ErrorMsgResponse(new Date(), errorMsgDescription);

        return new ResponseEntity<>(errorMsg, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);

    }

}