package com.mobileapp.photoappapialbums.shared;

import java.io.Serializable;

public class AlbumDto implements Serializable {

    private static final long serialVersionUID = 3109549160906340957L;

    private String albumId;
    private String name;
    private String description;

    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
