package com.mobileapp.photoappapialbums.service.impl;

import com.mobileapp.photoappapialbums.data.Album;
import com.mobileapp.photoappapialbums.shared.AlbumDto;
import com.mobileapp.photoappapialbums.exception.AlbumServiceException;
import com.mobileapp.photoappapialbums.repo.AlbumRepo;
import com.mobileapp.photoappapialbums.service.AlbumService;
import com.mobileapp.photoappapialbums.ui.model.AlbumResponseModel;
import com.mobileapp.photoappapialbums.ui.model.ErrorMessages;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;
import java.util.UUID;

@Service
public class AlbumServiceImpl implements AlbumService {

    AlbumRepo albumRepo;

    @Autowired
    public AlbumServiceImpl(AlbumRepo albumRepo) {
        this.albumRepo = albumRepo;
    }

    @Override
    public AlbumResponseModel createAlbum(AlbumDto albumDetails) {

        albumDetails.setAlbumId(UUID.randomUUID().toString());

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        Album persistAlbumDetails = modelMapper.map(albumDetails, Album.class);

        Album savedAlbum = albumRepo.save(persistAlbumDetails);
        AlbumResponseModel returnAlbum = modelMapper.map(savedAlbum, AlbumResponseModel.class);
        return returnAlbum;
    }

    @Override
    public List<AlbumResponseModel> getAlbums(int page, String sort, int limit) {

        Pageable pageableRequest = PageRequest.of(page, limit, Sort.Direction.valueOf(sort), "name");
        Page<Album> albumsListPage = albumRepo.findAll(pageableRequest);
        List<Album> albums = albumsListPage.getContent();

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        Type listType = new TypeToken<List<AlbumResponseModel>>() {
        }.getType();

        return modelMapper.map(albums, listType);
    }

    @Override
    public List<AlbumResponseModel> getUserAlbums(String userId, int page, String sort, int limit) {

        Pageable pageableRequest = PageRequest.of(page, limit, Sort.Direction.valueOf(sort), "name");
        Page<Album> pageableAlbumList = albumRepo.findByUserId(userId, pageableRequest);
        List<Album> userAlbumList = pageableAlbumList.getContent();

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        Type listType = new TypeToken<List<AlbumResponseModel>>() {
        }.getType();

        return modelMapper.map(userAlbumList, listType);
    }

    @Override
    public AlbumResponseModel getAlbum(String userId, String albumId) {

        Album album = albumRepo.findByUserIdAndAlbumId(userId, albumId)
                .orElseThrow(() -> new AlbumServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMsg() + albumId,
                        HttpStatus.valueOf(500)));

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        return modelMapper.map(album, AlbumResponseModel.class);

    }

    @Override
    public AlbumResponseModel updateAlbum(String userId, String albumId, Album albumDetails) {

//        Album savedAlbum = albumRepo.findByAlbumId(albumId)
//                .map(album -> albumRepo.save(albumDetails))
//                .orElseThrow(() -> new AlbumWsException("can't find album for this id: " + albumId,
//                        HttpStatus.valueOf(500)));
//
//        ModelMapper modelMapper = new ModelMapper();
//        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
//        return modelMapper.map(savedAlbum, AlbumResponseModel.class);

        String albumName = albumDetails.getName();
        String albumDescription = albumDetails.getDescription();
        albumRepo.updateAlbum(albumName, albumDescription, albumId);

        AlbumResponseModel updatedAlbum = getAlbum(userId, albumId);
        return updatedAlbum;
    }

    @Override
    public Long deleteAlbum(String albumId) {

        Long count = albumRepo.deleteByAlbumId(albumId)
                .orElseThrow(() -> new AlbumServiceException(ErrorMessages.COULD_NOT_DELETE_RECORD.getErrorMsg(),
                        HttpStatus.valueOf(500)));
        return count;
    }
}