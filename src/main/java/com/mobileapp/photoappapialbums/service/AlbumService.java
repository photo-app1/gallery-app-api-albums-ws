package com.mobileapp.photoappapialbums.service;

import com.mobileapp.photoappapialbums.data.Album;
import com.mobileapp.photoappapialbums.shared.AlbumDto;
import com.mobileapp.photoappapialbums.ui.model.AlbumResponseModel;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface AlbumService {

    AlbumResponseModel createAlbum(AlbumDto albumDetails);

    List<AlbumResponseModel> getAlbums(int page, String sort, int limit);

    AlbumResponseModel getAlbum(String userId, String albumId);

    AlbumResponseModel updateAlbum(String userId, String albumId, Album albumDetails);

    Long deleteAlbum(String albumId);

    List<AlbumResponseModel> getUserAlbums(String userId, int page, String sort, int limit);
}
