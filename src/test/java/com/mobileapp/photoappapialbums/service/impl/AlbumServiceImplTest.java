package com.mobileapp.photoappapialbums.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AlbumServiceImplTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void createAlbum() {
    }

    @Test
    void getAlbums() {
    }

    @Test
    void getUserAlbums() {
    }

    @Test
    void getAlbum() {
    }

    @Test
    void updateAlbum() {
    }

    @Test
    void deleteAlbum() {
    }
}