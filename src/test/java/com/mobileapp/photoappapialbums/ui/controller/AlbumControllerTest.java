package com.mobileapp.photoappapialbums.ui.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AlbumControllerTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void createAlbum() {
    }

    @Test
    void getAlbums() {
    }

    @Test
    void getUserAlbums() {
    }

    @Test
    void getAlbum() {
    }

    @Test
    void updateAlbum() {
    }

    @Test
    void deleteAlbum() {
    }
}