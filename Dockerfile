FROM openjdk:11
VOLUME /tmp
COPY build/libs/gallery-app-api-albums-ws-0.0.1-SNAPSHOT.jar GalleryAppApiAlbumsWs.jar
ENTRYPOINT ["java","-jar","GalleryAppApiAlbumsWs.jar"]